#!/bin/bash
declare -x ANDROID_JACK_VM_ARGS="-Dfile.encoding=UTF-8 -XX:+TieredCompilation -Xmx30G"
declare -x OUT_DIR_COMMON_BASE=/root/output

if [ -d "$HOME/bin" ] ; then
    PATH="/root/bin:$PATH"
fi

git config --global user.name "kmahyyg"
git config --global user.email "kmahyygyyg@gmail.com"
hostnamectl set-hostname aosp.kmahyyg.xyz
mkdir /root/output
mkdir /tmp/.ccache

export RR_BUILDTYPE="Official"
export WITH_ROOT_METHOD="magisk"
export days_to_log=5

alias mka="make"
declare -x USE_CCACHE=1
echo "declare -x USE_CCACHE=1" >> /root/.bashrc
export CCACHE_DIR="/tmp/.ccache"
declare -x CCACHE_DIR="/tmp/.ccache"
cd /RR
. prebuilts/misc/linux-x86/ccache/ccache -M 75G
git config --global push.default simple

#exit 0
