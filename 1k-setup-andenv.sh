#!/bin/bash
# Get the sources.list
#sh ./gpgkeyadd.list.jessie.sh

ulimit -n 51200
dpkg-reconfigure tzdata
echo 'Acquire::ForceIPv4 "true";' | sudo tee /etc/apt/apt.conf.d/99force-ipv4
#rm -f /etc/apt/sources.list
#cd /etc/apt
#wget https://c3.55aiguo.xyz/andenv/sources.list.ubt1604 | tee /root/setup.log1
cp -f ./sources.list.ubt1604 /etc/apt/sources.list

apt-get update -y | tee /root/setup.log1
apt-get upgrade -y
# Install openjdk 8
apt-get install -y openjdk-8-jdk ntpdate screen sudo | tee /root/setup.log1
ntpdate pool.ntp.org
# Create home/bin
mkdir -p /root/bin
curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > /root/bin/repo | tee /root/setup.log1
chmod a+x /root/bin/repo
cp -af /root/bin/repo /usr/bin/
# Install Dependencies
apt-get install -y python-networkx libnss3 bc bison build-essential curl flex g++-multilib gcc-multilib git gnupg gperf imagemagick lib32ncurses5-dev  | tee /root/setup.log1
apt-get install -y lib32readline6-dev lib32z1-dev libesd0-dev liblz4-tool libncurses5-dev libsdl1.2-dev libwxgtk3.0-dev | tee /root/setup.log1
apt-get install -y libxml2 libxml2-utils lzop pngcrush schedtool squashfs-tools xsltproc zip zlib1g-dev | tee /root/setup.log1
# Update Env

if [ -d "$HOME/bin" ] ; then
    PATH="/root/bin:$PATH"
fi

declare -x ANDROID_JACK_VM_ARGS="-Dfile.encoding=UTF-8 -XX:+TieredCompilation -Xmx30G"
declare -x OUT_DIR_COMMON_BASE=/root/output
echo 'See here for ccache settings: https://source.android.com/source/initializing.html'
git config --global user.name "kmahyyg"
git config --global user.email "kmahyygyyg@gmail.com"
hostnamectl set-hostname aosp.kmahyyg.xyz
mkdir /root/output
mkdir /tmp/.ccache
source ~/.profile
update-grub
sync

#exit 0

# more details: http://wiki.lineageos.org/ido_build.html
# about rr: https://github.com/ResurrectionRemix/platform_manifest/blob/nougat/README.mkdn
