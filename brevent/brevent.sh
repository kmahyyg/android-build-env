#!/bin/bash
cd ./brevent
java -Xms1g -jar baksmali-2.2b4.jar d ./services.jar -o services_ori
java -Xms1g -jar baksmali-2.2b4.jar d ./br-v3.1.2.apk -o apk
cp ./assets/patch.py ./patch.py -af
chmod a+x ./patch.py
python ./patch.py -a apk -s services_ori
sleep 10
java -Xms1g -jar smali-2.2b4.jar a -o classes.dex services_ori
jar -cvf services-p.jar classes.dex
echo "Successfully Generated patched File!"
exit 0
