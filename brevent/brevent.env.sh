#!/bin/bash
mv ./sources.list.jessie /etc/apt/sources.list -f
apt-get clean -y
apt-get update -y
apt-get install openjdk-8-jre openjdk-8-jdk -y -t jessie-backports 
apt-get install git -y
git clone https://github.com/kmahyyg/Brevent-Full-Archive ./brevent
cd ./brevent
wget https://bitbucket.org/JesusFreke/smali/downloads/smali-2.2b4.jar
wget https://bitbucket.org/JesusFreke/smali/downloads/baksmali-2.2b4.jar
apt-get install python2.7 -y

echo "Upload files , please."
sleep 2

exit 0

