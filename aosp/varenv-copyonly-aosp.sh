#!/bin/bash
declare -x ANDROID_JACK_VM_ARGS="-Dfile.encoding=UTF-8 -XX:+TieredCompilation -Xmx2G"
declare -x OUT_DIR_COMMON_BASE=/mnt/aosp/output

if [ -d "$HOME/bin" ] ; then
    PATH="/root/bin:$PATH"
fi

git config --global user.name "kmahyyg"
git config --global user.email "kmahyygyyg@gmail.com"
hostnamectl set-hostname aosp.kmahyyg.cf
mkdir /mnt/aosp/output
#mkdir /tmp/.ccache

declare -x USE_CCACHE=0
echo "declare -x USE_CCACHE=0" >> /root/.bashrc
#export CCACHE_DIR="/tmp/.ccache"
#declare -x CCACHE_DIR="/tmp/.ccache"
#. prebuilts/misc/linux-x86/ccache/ccache -M 50G
git config --global push.default simple

exit 0
