#!/bin/bash
mkdir /RR
cd /RR
repo init -u https://github.com/ResurrectionRemix/platform_manifest.git -b nougat
mkdir /RR/.repo/local_manifests
cd /RR/.repo/local_manifests
wget https://raw.githubusercontent.com/TheMuppets/manifests/cm-14.1/muppets.xml
cd /RR
repo sync -f --force-sync --no-clone-bundle

declare -x USE_CCACHE=1
echo "declare -x USE_CCACHE=1" | tee /root/.bashrc
export CCACHE_DIR="/tmp/.ccache"
declare -x CCACHE_DIR="/tmp/.ccache"
chmod +x /RR/prebuilts/misc/linux-x86/ccache/ccache
/RR/prebuilts/misc/linux-x86/ccache/ccache -M 75G
cd /RR
mka clobber
make clean
. build/envsetup.sh && brunch ido && mka -j32 bacon


