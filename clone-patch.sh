#!/bin/bash
mkdir /RR
cd /RR
repo init -u https://github.com/ResurrectionRemix/platform_manifest.git -b nougat
mkdir /RR/.repo/local_manifests
cd /RR/.repo/local_manifests
cp /root/android-build-env/trigger.xml ./trigger.xml -af
# wget https://raw.githubusercontent.com/TheMuppets/manifests/cm-14.1/muppets.xml
cd /RR
repo sync -f -j16  --force-sync --no-clone-bundle
repo sync

declare -x USE_CCACHE=1
echo "declare -x USE_CCACHE=1" | tee /root/.bashrc
export CCACHE_DIR="/tmp/.ccache"
declare -x CCACHE_DIR="/tmp/.ccache"
chmod +x /RR/prebuilts/misc/linux-x86/ccache/ccache
/RR/prebuilts/misc/linux-x86/ccache/ccache -M 75G
cd /RR
alias mka=make
mka clobber
make clean
export RR_BUILDTYPE=Official
export WITH_ROOT_METHOD="magisk"
export days_to_log=5
. build/envsetup.sh && brunch ido && make -j16 bacon


