#!/bin/bash
cd /mnt/aosp/work
mkdir ./ndk
wget https://dl.google.com/android/repository/android-ndk-r14-linux-x86_64.zip
apt-get update -y
apt-get install unzip -y
unzip ./android-ndk-r14-linux-x86_64.zip -d ./ndk/
exit 0
