#!/bin/bash
swapon -s
free -m
df -h
sleep 30
dd if=/dev/zero of=/var/swap.1 bs=1M count=4096
mkswap /var/swap.1
swapon /var/swap.1
echo "/var/swap.1 swap swap defaults 0 0" | tee /etc/fstab
exit 0
